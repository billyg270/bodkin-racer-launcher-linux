# Bodkin Racer Launcher - Linux
This is a script that downloads / updates Bodkin Racer and runs it.

## Requirements
To run this script you will need git installed on your system. To do this on Debian based systems, use `sudo apt install git`.

## Usage
Download the script `BodkinRacer.sh` to a location of your choice, then run the script. You may need to mark the script as executable, this can be done through the terminal using `chmod 777 BodkinRacer.sh`.

To play the snapshot version of the game, use `./BodkinRacer.sh --snapshot`.

## Notes
The game will be downloaded to `/home/(your username)/.config/BodkinRacer/game`. You can change this by modifying the script to your liking.